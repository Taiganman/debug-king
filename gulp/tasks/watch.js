module.exports = () => {
    // Watchers
    $.gulp.task('watch', done => {
        $.gulp.watch($.path.src.pug + '**/*.pug', $.gulp.task('pug'));
        $.gulp.watch($.path.src.scss + '**/*.scss', $.gulp.task('styles'));
        $.gulp.watch($.path.src.es + '**/*.es', $.gulp.task('babel'));
        done();
    });
};

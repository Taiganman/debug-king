// es
const babel = require('gulp-babel');
const eslint = require('gulp-eslint');

module.exports = () => {
    //babel
    $.gulp.task('scripts', done => {
        return $.gulp
            .src($.path.src.es + '**/*.js')
            .pipe($.debug())
            .pipe(
                $.plumber({
                    errorHandler: $.notify.onError(
                        'Error BABEL: <%= error.message %>'
                    )
                })
            )
            .pipe(eslint.format())
            .pipe(eslint.failAfterError())
            .pipe(babel())
            .pipe($.plumber.stop())
            .pipe($.gulp.dest($.path.dev.js))
            .pipe(
                $.browserSync.reload({
                    stream: true
                })
            );
    });

    $.gulp.task('scripts:prod', () => {
        return $.gulp
            .src($.path.dev.js + '**/*.js')
            .pipe($.debug())
            .pipe($.plumber())
            .pipe($.plumber.stop())
            .pipe($.gulp.dest($.path.prod.js));
    });
};

'use strict';

/*
 * ページ内リンクスクロール
 */
$.fn.pageScroll = function(options){
    return $(this).each(function(){
        new $.pageScroll(this, $.extend({
            'position': 0,//停止位置の調整
            'fnc': undefined,//beforeScroll関数
        }, options));
    });
};
$.pageScroll = function(elem, options){
    this.$elem = $(elem);
    this.options = options;
    this.fnc = this.options['fnc'];
    this.afterfnc = this.options['afterfnc'];
    this.href = this.$elem.attr("href");
    this.href = this.href == "#" || this.href == "" ? 'html' : this.href;
    if(this.href.split('#').length !== 1){
        this.href = '#' + this.href.split('#')[1];
    }else if(this.href != 'html'){
        return true;
    }
    this.speed = 500;
    this.setEvent();
};
$.pageScroll.prototype = {
    setEvent: function(){
        var self = this;
        self.$elem.click(function(){
            if(self.fnc !== undefined){
            	self.fnc();
            }
            return self.startScroll();
    });
    },
    startScroll: function(){
        var self = this;
        this.target = $(this.href);
        if(this.target.length > 0){
            this.position = this.target.offset().top + this.options.position;
            $('html, body').animate({scrollTop:this.position}, self.speed, 'swing');
            return false;
        }else{
        	return true;
        }
    }
};
/*
 * 画面内に要素を固定、一定要素まで来たらそこへ固定（pagetopなどに使用）
 */
$.fn.fixedElement = function(options){
    return $(this).each(function(){
        new fixedElement(this, $.extend({
            'position': 'bottom',//停止位置の調整
            'fixedelement': 'footer',//停止させたい要素名
            'adjust': { window: 768, num: 10}//調整値
        }, options));
    });
};
class fixedElement{
	constructor(elem, options){
		this._$btn = $(elem);
		this._options = options;
		this._BottomPos = parseInt(this._$btn.css('bottom'));
		$(window).on('scroll', () => {
			this.chkPos();
		});
	}
	chkPos(){
		const WindowHeight =　window.innerHeight;
		const PageHeight = $(document).height();
		const ScrollTop = $(window).scrollTop();
		const footerHeight = $(this._options.fixedelement).height();
		const MoveTopBtn = WindowHeight + ScrollTop + footerHeight - PageHeight;
		let adjust = !window.matchMedia( '(max-width: '+ this._options.adjust.window +'px)' ).matches ? this._options.adjust.num : 0;

		if(this._options.position == 'top'){
			adjust = adjust + this._$btn.height();
		}else if(this._options.position == 'center'){
			adjust = adjust + (this._$btn.height()/2);
		}
		if((ScrollTop > 630) && !this._$btn.hasClass('show')){
			this._$btn.addClass('show');
		}else if((ScrollTop < 200) && this._$btn.hasClass('show')){
			this._$btn.removeClass('show');
		}

		const chkfooter = ScrollTop >= PageHeight - WindowHeight - footerHeight + this._BottomPos - adjust;
		if(chkfooter) {
			this._$btn.css({ bottom: MoveTopBtn + adjust });
		}else {
			this._$btn.css({ bottom: this._BottomPos });
		}
	}

}
//URLにハッシュがついていればその位置までアニメーションでスクロールする
class scrollAfterLoading{
	constructor(){
		const hash = window.location.hash;
		$('html, body').css('visibility', 'hidden');//一旦非表示に
		if(hash.length < 2){
		    $('html, body').css('visibility', '');
		}else{
		    setTimeout(function(){
		        $('html, body').scrollTop(0).css('visibility', '').delay(500).queue(function(){
		             var position = $(hash).offset().top;
		            $('html, body').animate({scrollTop:position}, 1000, 'swing').dequeue();
		        });
	        }, 0);
		}
	}
}

//タブ切り替え
class tabContent{
	constructor(){
		const self = this;
		$('.tab').each(function(){
			self.setTab(this);
		});
	}
	setTab(elem){
		const self = this;
		const $elem = $(elem);
		const $tab = $elem.find('.tab__tab:first').find('.tab__link');
		const $contain = $elem.find('.tab__body:first').find('.tab__container');
		const startnum = $elem.data('tabStart') === void 0 ? 0 : $elem.data('tabStart');
		$tab.on('click', function(e){
			e.preventDefault();
			if(!!$(this).data('tabTarget')){
				$tab.removeClass('tab__link--focus');
				$(this).addClass('tab__link--focus');
				$contain.hide().filter('[rel="'+$(this).data('tabTarget')+'"]').fadeIn().trigger('tab.change');
			};
		});
		$tab.eq(startnum).trigger('click');
	}
}

//文字サイズ変更
class changeFontSize{
	constructor(){
		const $btn = $('.fontsizechange__link');
		$('.fontsizechange__link').on('click', function(e){
			e.preventDefault();
			$btn.removeClass('fontsizechange__link--focus');
			$(this).addClass('fontsizechange__link--focus');
			$('html').removeClass().addClass($(this).data('fontSize')).trigger('fontSize.change');

			$.cookie('fontSize', $(this).data('fontSize'));
		});
		if($.cookie('fontSize') !== void 0){
			$('.fontsizechange__link[rel="'+ $.cookie('fontSize') +'"]').trigger('click');
		};
	}
}

//ローディング
class pageloading{
	constructor(){
		$('body').append('<div class="loading"></div>').trigger('pageLoad.start');
		const startOnLoadImage = this.startOnLoadImage;
		const $img = $('img');
		startOnLoadImage($img).done(()=>{
			this.loaderClose();
		});
	}
	loaderClose(){
		$(".loading").fadeOut('slow');
		$('.wrap').addClass('show');
		setTimeout(() => {$('body').trigger('pageLoad.loaded')}, 100);
	}
	startOnLoadImage($target){
		var d = new $.Deferred();
		var loaded = 0;
		var max = $target.length;
		$target.each(function() {
		  var targetObj = new Image();
		  $(targetObj).on('load', function(){
			loaded++;
			if (loaded == max){
				setTimeout(function(){
					d.resolve();
				}, 2000);
			}
		  });
		  targetObj.src = this.src;
		});
		return d.promise();
	};
}


//フォーム周り
class formSetting{
	constructor(){
		this.$form = $('form');
		//アクセシビリティ対策
		this.$form.on('focus', 'select', (e) => {
			$(e.target).parent().addClass("selectwrapper--focus");
		}).on('blur', 'select', (e) => {
			$(e.target).parent().removeClass("selectwrapper--focus");
		}).on('focus', '.radiowrapper > input[type="radio"]', (e) => {
			$(e.target).parent().addClass("radiowrapper--focus");
		}).on('blur', '.radiowrapper >  input[type="radio"]', (e) => {
			$(e.target).parent().removeClass("radiowrapper--focus");
		}).on('focus', '.checkwrapper > input[type="checkbox"]', (e) => {
			$(e.target).parent().addClass("checkwrapper--focus");
		}).on('blur', '.checkwrapper >  input[type="checkbox"]', (e) => {
			$(e.target).parent().removeClass("checkwrapper--focus");
		});
	}
	
}


//SPメニュー
class spMenu{
	constructor(){
		this._btn = $('a[data-spmenu]');
		this._scrollpos;
		this._menuopen;

		this._btn.on('click', () => {
			this.ctrlMenu();
		});
	}
	ctrlMenu(){
		this._menuopen = $('body').hasClass('activemenu');
		if(!this._menuopen){//メニュー表示
			this._scrollpos = $(window).scrollTop();//スクロール位置を格納
			$('body').addClass('activemenu').trigger('spMenu.open');
		}else{//メニュー閉じる
			$('body').removeClass('activemenu').trigger('spMenu.close');
			$(window).scrollTop(this._scrollpos)
		}
		this._btn.toggleClass('active');
	}
}

(()=>{
	$('.footer__pagetop__link').fixedElement({fixedelement: '.footer__wrapper', position: 'bottom', adjust: {window: '1px', num: -20}}).pageScroll();

	new tabContent();
	new formSetting();

	//new scrollAfterLoading();
})();